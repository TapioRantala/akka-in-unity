using Akka.Actor;

public class PlayerActorServer : PlayerActor
{
    public PlayerActorServer(string id) : base(id)
    {
    }

    protected override void OnReceive(object message)
    {
        switch (message)
        {
            case int x:
                // Yay we received an int!
                break;
            default:
                base.OnReceive(message);
                break;
        }
    }

    public static Props Props(string id)
    {
        return Akka.Actor.Props.Create(() => new PlayerActorServer(id));
    }
}
