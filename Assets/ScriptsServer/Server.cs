using Akka.Actor;

public class Server
{
    private readonly ActorSystem System;

    private Server()
    {
        System = ActorSystem.Create("server");
    }

    public IActorRef CreatePlayerActor(string id)
    {
        return System.ActorOf(PlayerActorServer.Props(id), $"Player {id}");
    }
}
