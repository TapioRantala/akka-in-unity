using Akka.Actor;

public class Client
{
    private readonly ActorSystem System;

    private Client()
    {
        System = ActorSystem.Create("client");
    }

    public IActorRef CreatePlayerActor(string id)
    {
        return System.ActorOf(PlayerActorClient.Props(id), $"Player {id}");
    }
}
