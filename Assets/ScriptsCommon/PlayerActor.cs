using Akka.Actor;
using Akka.Event;

public class PlayerActor : UntypedActor
{
    protected readonly string ID;
    protected readonly ILoggingAdapter Log = Context.GetLogger();

    protected PlayerActor(string id)
    {
        ID = id;
    }

    protected override void OnReceive(object message)
    {
        switch (message)
        {
            case int x:
                // Yay we received an int!
                break;
            default:
                // Received unknown message
                Log.Warning("Received unknown message");
                break;
        }
    }
}
